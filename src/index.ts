import fetch from 'node-fetch';

const DD_URL = 'https://www.duckdns.org/update';

export async function sendIp(token: string, domains: string, ip?: string): Promise<string> {
  const url = new URL(DD_URL);
  url.searchParams.append('token', token);
  url.searchParams.append('domains', domains);
  if (ip) url.searchParams.append('ip', ip);
  return fetch(url.toString()).then((response) => {
    if (!response.ok) {
      console.log(response.status);
      console.log(url);
    }
    return response.text();
  });
}
