#!/usr/bin/env node

import yargs from 'yargs';
import { sendIp } from './index';

const argv = yargs(process.argv.slice(2))
  .options({
    t: {
      alias: 'token',
      demandOption: true,
      describe: 'account token',
      type: 'string',
    },
    i: {
      alias: 'ip',
      demandOption: false,
      describe: 'Ip',
      type: 'string',
    },
    d: {
      alias: 'domain',
      demandOption: true,
      describe: 'account domain',
      type: 'string',
    },
  })
  .parse();

sendIp(argv.t, argv.d, argv.i).then((res) => {
  console.log(res);
});
