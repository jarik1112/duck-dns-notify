#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const yargs_1 = __importDefault(require("yargs"));
const index_1 = require("./index");
const argv = (0, yargs_1.default)(process.argv.slice(2))
    .options({
    t: {
        alias: 'token',
        demandOption: true,
        describe: 'account token',
        type: 'string',
    },
    i: {
        alias: 'ip',
        demandOption: false,
        describe: 'Ip',
        type: 'string',
    },
    d: {
        alias: 'domain',
        demandOption: true,
        describe: 'account domain',
        type: 'string',
    },
})
    .parse();
(0, index_1.sendIp)(argv.t, argv.d, argv.i).then((res) => {
    console.log(res);
});
