"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendIp = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
const DD_URL = 'https://www.duckdns.org/update';
async function sendIp(token, domains, ip) {
    const url = new URL(DD_URL);
    url.searchParams.append('token', token);
    url.searchParams.append('domains', domains);
    if (ip)
        url.searchParams.append('ip', ip);
    return (0, node_fetch_1.default)(url.toString()).then((response) => {
        if (!response.ok) {
            console.log(response.status);
            console.log(url);
        }
        return response.text();
    });
}
exports.sendIp = sendIp;
